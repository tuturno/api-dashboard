'use strict';
module.exports = function(grunt) {
  // Load all tasks
  require('load-grunt-tasks')(grunt);
  // Show elapsed time
  require('time-grunt')(grunt);

  var jsFileList = [
    'assets/js/_*.js'
  ];

  grunt.initConfig({
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: [
        'Gruntfile.js',
        'assets/js/_*.js',
        '!assets/*.min.js'
      ]
    },
    less: {
      devstyle: {
        files: {
          'assets/css/style.css': 'assets/less/style.less'
        },
        options: {
          compress: false,
          // LESS source map
          // To enable, set sourceMap to true and update sourceMapRootpath based on your install
          sourceMap: true,
          sourceMapFilename: 'assets/css/style.css.map',
          sourceMapRootpath: '/'
        }
      },
      devaddons: {
        files: {
          'assets/css/add-ons.css': 'assets/less/add-ons.less'
        },
        options: {
          compress: false,
          // LESS source map
          // To enable, set sourceMap to true and update sourceMapRootpath based on your install
          sourceMap: true,
          sourceMapFilename: 'assets/css/add-ons.css.map',
          sourceMapRootpath: '/'
        }
      },
      build: {
        files: {
          'assets/css/style.min.css': 'assets/less/style.less',
          'assets/css/add-ons.min.css': 'assets/less/add-ons.less'
        },
        options: {
          compress: true
        }
      }
    },
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: [jsFileList],
        dest: 'assets/js/scripts.js',
      },
    },
    uglify: {
      dist: {
        files: {
          'assets/js/scripts.min.js': [jsFileList]
        }
      }
    },
    autoprefixer: {
      options: {
        browsers: ['last 2 versions', 'ie 8', 'ie 9', 'android 2.3', 'android 4', 'opera 12']
      },
      dev: {
        options: {
          map: {
            prev: 'assets/css/'
          }
        },
        src: ['assets/css/style.css','assets/css/add-ons.css']
      },
      build: {
        src: ['assets/css/style.min.css','assets/css/add-ons.min.css']
      }
    },
    modernizr: {
      build: {
        devFile: 'assets/plugins/modernizr/modernizr.js',
        outputFile: 'assets/js/modernizr.min.js',
        files: {
          'src': [
            ['assets/js/scripts.min.js'],
            ['assets/css/style.min.css'],
            ['assets/css/add-ons.min.css']
          ]
        },
        extra: {
          shiv: false
        },
        uglify: true,
        parseFiles: true
      }
    },
    watch: {
      less: {
        files: [
          'assets/less/*.less',
          'assets/less/**/*.less'
        ],
        tasks: ['less:devstyle', 'less:devaddons', 'autoprefixer:dev']
      },
      js: {
        files: [
          jsFileList,
          '<%= jshint.all %>'
        ],
        tasks: ['jshint', 'concat']
      },
      livereload: {
        // Browser live reloading
        // https://github.com/gruntjs/grunt-contrib-watch#live-reloading
        options: {
          livereload: false
        },
        files: [
          'assets/css/style.css',
          'assets/css/add-ons.css',
          'assets/js/scripts.js'
        ]
      }
    }
  });

  // Register tasks
  grunt.registerTask('default', [
    'dev'
  ]);
  grunt.registerTask('dev', [
    'jshint',
    'less:devstyle',
    'less:devaddons',
    'autoprefixer:dev',
    'concat'
  ]);
  grunt.registerTask('build', [
    'less:build',
    'autoprefixer:build',
    'uglify',
    'modernizr'
  ]);
};
