Turns API Dashboard v.0.1.0
===========================

Dashboard to manage all settings on the Turn API generated from tuturno-core project.

To login in the admin, **please use any user and pass.**
Currently it is not connected to any database, just it is validating fields content.