<?php
/**
 * Middleware to validate existing login or redirect to show the login page.
 **/
function validateUserLogin( \Slim\Route $router ) {
	$app = \Slim\Slim::getInstance();

	$pattern = explode("/", $router->getPattern());

	if( !isset($_SESSION['user']) ) {
		$app->redirect(ROOT_URI.'/login');
	}
}