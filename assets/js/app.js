var app = angular.module('DashboardApp', []);

function sanitizeText(input) {
    if (input) {
        return input.replace(/\s+/g, '-').toLowerCase();    
    }
}

app.filter('spaceless',function() {
    return sanitizeText(input);
});