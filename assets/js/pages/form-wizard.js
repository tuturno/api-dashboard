function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

angular.module('DashboardApp').controller("wizardController", ["$scope", function($scope){
	
	$scope.company = {};
	$scope.queues = [{name:'', code:'', prefix:'', maxTurn:''}];
	$scope.operators = [{username:'', email:'', password:''}];
	
	$scope.encodeCompanyName = function() {
		$scope.company.code = sanitizeText( $scope.company.name );
	};


	/** Queues Functions **/
	$scope.encodeQueueName = function(index) {
		$scope.queues[index].code = sanitizeText( $scope.queues[index].name );
	};

	$scope.addNewQueue = function() {
		$scope.queues.push({name:'', code:'', prefix:'', maxTurn:''});
		
		$scope.$watch('queues', function(newValue, oldValue){
			$('.input-max-turn').mask("9?999");
			resetInputValidations();
		});
	};

	$scope.removeQueue = function(index) {
		if($scope.queues.length>1) {
			$scope.queues.splice(index, 1);
		}
	}


	/** Operators Functions **/
	$scope.addNewOperator = function() {
		$scope.operators.push({username:'', email:'', password:''});
		
		$scope.$watch('operators', function(newValue, oldValue){
			resetInputValidations();
		});
	};

	$scope.removeOperator = function(index) {
		if($scope.operators.length>1) {
			$scope.operators.splice(index, 1);
		}
	}
}]);

function resetInputValidations() {
	function resetInputField(){		
		if($(this).val()) {
			$(this).parent().parent().removeClass('has-error');
		}		
	}

	$('.operator-input, .queue-input').keyup(resetInputField);
	$('.operator-input, .queue-input').blur(resetInputField);
}

$(document).ready(function() {

	angular.bootstrap(document, ['DashboardApp']);
	
	$('#ccnumber-w1').mask("9999 9999 9999 9999");
	$('.input-max-turn').mask("9?999");
	
	/* ---------- Wizard ---------- */
	$('#email-w1').keyup(function(){
		
		if(isEmail($(this).val())) {
			$(this).parent().parent().removeClass('has-error');
		}
		
	});

	resetInputValidations();
	
	$('#ccnumber-w1').keyup(function(){
		
		var getCCNumber = $(this).val();
		getCCNumber = getCCNumber.replace(/ /g,'').replace(/_/g,'');
		
		if(getCCNumber.length == 16) {
			$(this).parent().parent().removeClass('has-error');
		}
		
	});
	
	$('#cvv-w1').keyup(function(){
		
		if($(this).val().length === 3) {
			$(this).parent().parent().removeClass('has-error');
		} else {
			$(this).parent().parent().addClass('has-error');
		}
		
	});


	$('#wizard1').bootstrapWizard({
		'nextSelector': '.button-next',
		'previousSelector': '.button-previous',
		onNext: function(tab, navigation, index) {

			var bugs = 0;

			if(index===1) {
				if(!$('#company-w1').val()) {
					$('#company-w1').parent().addClass('has-error');
					bugs = 1;
				}

			} else if(index===2) {

				$('.queue-input').each(function(index){
					if(!$(this).val()) {
						$(this).parent().parent().addClass('has-error');
						bugs = 1;
					}
				});
				
			} else if(index===3) {
				
				$('.operator-input').each(function(index) {
					if(!$(this).val()) {
						$(this).parent().parent().addClass('has-error');
						bugs = 1;
					} else {
						if( $(this).hasClass('email-input') && !isEmail($(this).val()) ) {
							$(this).parent().parent().addClass('has-error');
							bugs = 1;
						}
					}
				});
				
			}

			if( bugs === 1) {
				return false;
			}

		}, onTabShow: function(tab, navigation, index) {
			var $total = navigation.find('li').length;
			var $current = index+1;
			var $percent = ($current/$total) * 100;
			$('#wizard1').find('.progress-bar').css({width:$percent+'%'});
			
			$('#wizard1 > .steps li').each( function (index) {
				$(this).removeClass('complete');
				index += 1;
				if(index < $current) {
					$(this).addClass('complete');
				}
			});
			
			if($current >= $total) {
				$('#wizard1').find('.button-next').hide();
				$('#wizard1').find('.button-finish').show();
			} else {
				$('#wizard1').find('.button-next').show();
				$('#wizard1').find('.button-finish').hide();
			}
		}
	});
		
	
	/* ---------- Datapicker ---------- */
	$('.datepicker').datepicker();

	/* ---------- Choosen ---------- */
	$('[data-rel="chosen"],[rel="chosen"]').chosen();

	/* ---------- Placeholder Fix for IE ---------- */
	$('input, textarea').placeholder();

	/* ---------- Auto Height texarea ---------- */
	$('textarea').autosize();
});