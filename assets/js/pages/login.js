jQuery(document).ready(function($){
	
	var error_msg = $('#error-msg').html();
	if( error_msg!==undefined && error_msg.trim().length>0 ) {
		$('#error-msg').removeClass('hidden');
	}


	var info_msg = $('#info-msg').html();
	if( info_msg!==undefined &&  info_msg.trim().length>0 ) {
		console.log( info_msg );
		$('#info-msg').removeClass('hidden');
	}


	/* ---------- Placeholder Fix for IE ---------- */
	/*$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue'
	});*/
});