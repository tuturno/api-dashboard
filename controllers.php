<?php

function dashboardController() {
	$app = \Slim\Slim::getInstance();
	$app->render('index.php');
}


function logoutController() {
	$app = \Slim\Slim::getInstance();
	
	unset( $_SESSION['user'] );

	$app->redirect(ROOT_URI);
}


function loginController() {
	$app = \Slim\Slim::getInstance();
	$app->render('page-login.php');
}

function loginProcessController() {
	$app = \Slim\Slim::getInstance();

	$_SESSION['user'] = array('user_id'=>1, 'username'=>'jalamprea', 'fullname'=>'Julian Lamprea');
	
	$app->redirect(ROOT_URI);
}


function registerController() {
	$app = \Slim\Slim::getInstance();
	$app->render('page-register.php');
}

function registerProcessController() {
	$app = \Slim\Slim::getInstance();

	$username = $app->request->post('username');
	$fullname = $app->request->post('fullname');
	$email 	  = $app->request->post('email');
	$password = $app->request->post('userpass');

	$fields = array(
            'username'=>$username,
            'fullName'=>$fullname,
            'pass'=>$password,
            'passConfirm'=>$password,
            'email'=>$email
        );

	//echo "<pre>Data: ".print_r(json_encode($fields), true)."</pre>";

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result 
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result

	//set the url, number of POST vars, POST data
	curl_setopt($ch, CURLOPT_URL, $app->SERVER_URL."/userRegister/createAccount.json");
	curl_setopt($ch, CURLOPT_POST, count($fields));
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

	// Fetch and return content, save it.
	$raw_data = curl_exec($ch);
	curl_close($ch);

	if($raw_data) {
		// If the API is JSON, use json_decode.
		$res = json_decode($raw_data);
		if(isset($res->errors)) {
			$errors_msg = '';
			foreach ($res->errors as $error) {
				$errors_msg .= $error->message."\n<br>\n";
			}
			$app->flash('error', $errors_msg);


			foreach ($fields as $key => $value) {
				$app->flash($key, $value);
			}

			
			$app->response->redirect('/register', 303);
		} else {
			$app->flash('info', 'Welcome to Neverlines API, please login with your new credentials.');
			$app->response->redirect('/login', 303);
		}
	} else {
		foreach ($fields as $key => $value) {
			$app->flash($key, $value);
		}

		$app->flash('error', 'Ops! we are having network issues, our servers are down :( Please try again later.');
		$app->response->redirect('/register', 303);
	}
}