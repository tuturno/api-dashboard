<?php
/**
 * TuTurno-Core API Dashboard
 * @version 1.0.0
 */

require_once 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app_mode = ($_SERVER['SERVER_NAME']==='dashboard.mobileturns.com') ? 'production' : 'development';
$app = new \Slim\Slim( array('mode' => $app_mode) );

// Only invoked if mode is "production"
$app->configureMode('production', function () use ($app) {
    $app->config(array(
		'cookies.encrypt' => true,
		'cookies.lifetime' => '60 minutes',
        'log.enable' => true,
		'log.level' => \Slim\Log::ERROR,
        'debug' => false,
		'templates.path' => './templates' 
    ));
});

// Only invoked if mode is "development"
$app->configureMode('development', function () use ($app) {
    $app->config(array(
        'log.enable' => false,
        'log.level' => \Slim\Log::ERROR,
        'debug' => true,
		'templates.path' => './templates' 
    ));
});

$app->setName('API-dashboard');

$app->add(new \Slim\Middleware\SessionCookie(array(
    'expires' => '30 minutes',
    'path' => '/',
    'domain' => null,
    'secure' => false,
    'httponly' => false,
    'name' => 'dashboard_guess',
    'secret' => 'czBVbjdVY2k4OA==',
    'cipher' => MCRYPT_RIJNDAEL_256,
    'cipher_mode' => MCRYPT_MODE_CBC
)));


define('ROOT_DIR', dirname(__FILE__));
define('ROOT_URI', 'http://' . $_SERVER['SERVER_NAME']);

// Utilities
require_once('access-filters.php');

// define required controllers
require_once('controllers.php');

/**--------Cors--------**/
require_once 'Slim/Middleware/Cors.php';

 $corsOptions = array(
     "origin" => "*",
     "allowMethods" => array("POST", "GET")
 );

$cors = new \CorsSlim\CorsSlim($corsOptions);
$app->add($cors);
/**--------Cors--------**/


/** Function for the Authenthication Callback of the Pusher Library **/
function pusherAuth( ) {
    
    $socket_id = $_POST['socket_id'];
    $channel_name = $_POST['channel_name'];
    
    $app_key = 'f1aaaca2a3093fad131b';
    $secret_key = 'c12a486b7e1f8f31b51d';

    $hash = hash_hmac("sha256", $socket_id.':'.$channel_name, $secret_key);

    $response = array('auth'=>$app_key.':'.$hash);

    echo json_encode($response);
    //die();
}

$app->map('/pusher/auth/', 'pusherAuth' )->via('GET', 'POST');
//$app->post('/pusher/auth/', 'pusherAuth' );


// Routers Configurations
$app->get('/', 'validateUserLogin', 'dashboardController');
$app->get('/login/', 'loginController' );
$app->get('/logout/', 'logoutController' );
$app->get('/register/', 'registerController' );
$app->post('/login/process/', 'loginProcessController' );
$app->post('/register/process/', 'registerProcessController' );

//echo "<pre>".print_r($_SERVER, true)."</pre>";
if( $_SERVER['REMOTE_ADDR']==='127.0.0.1' ) {
    $app->SERVER_URL = 'http://localhost:8080/services';
} else {
    $app->SERVER_URL = 'undefined... :(';
}

$app->notFound(function () use ($app) {
    $app->render('page-404.html');
});

$app->error(function (\Exception $e) use ($app) {
    $app->render('page-500.html');
});


$app->run();