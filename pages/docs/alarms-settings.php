<div class="row">
	<div class="col-lg-9">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2><i class="fa fa-font"></i><span class="break"></span>API Documentation</h2>
			</div>
			<div class="panel-body">
				  <div class="page-header">
					  <h1>Alarms and Notifications <small>Sending notifications to users and manage alarms</small></h1>
				  </div>     
				  <div class="row">            
					  <div class="col-lg-4">
						<h3>Sample text and paragraphs</h3>
						<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales at. Nulla tellus elit, varius non commodo eget, mattis vel eros. In sed ornare nulla. Donec consectetur, velit a pharetra ultricies, diam lorem lacinia risus, ac commodo orci erat eu massa. Sed sit amet nulla ipsum. Donec felis mauris, vulputate sed tempor at, aliquam a ligula. Pellentesque non pulvinar nisi.
						</p>
						<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales at. Nulla tellus elit, varius non commodo eget, mattis vel eros. In sed ornare nulla. Donec consectetur, velit a pharetra ultricies, diam lorem lacinia risus, ac commodo orci erat eu massa. Sed sit amet nulla ipsum. Donec felis mauris, vulputate sed tempor at, aliquam a ligula. Pellentesque non pulvinar nisi.
						</p>
					  </div>
					  <div class="col-lg-4">
						<h3>Example body text</h3>
						<p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
						<p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec sed odio dui.</p>
					  </div>
					  <div class="col-lg-4">
						<div class="well">
						  <h1>h1. Heading 1</h1>
						  <h2>h2. Heading 2</h2>
						  <h3>h3. Heading 3</h3>
						  <h4>h4. Heading 4</h4>
						  <h5>h5. Heading 5</h5>
						  <h6>h6. Heading 6</h6>
						</div>
					  </div>
				  </div><!--/row -->                           

				  <div class="row">
					  <div class="col-lg-12">
						  <h3>Example blockquotes</h3>
						  <div class="row">
							<div class="col-lg-6">
							  <p>Default blockquotes are styled as such:</p>
							  <blockquote>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante venenatis.</p>
								<small>Someone famous in <cite title="">Body of work</cite></small>
							  </blockquote>
							</div>
							<div class="col-lg-6">
							  <p>You can always float your blockquote to the right:</p>
							  <blockquote class="pull-right">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante venenatis.</p>
								<small>Someone famous in <cite title="">Body of work</cite></small>
							  </blockquote>
							</div>
						  </div>
					  </div>
				  </div>
				  <div class="row">
						<div class="col-lg-6">
						<h3>More Sample Text</h3>
						<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales at. Nulla tellus elit, varius non commodo eget, mattis vel eros. In sed ornare nulla. Donec consectetur, velit a pharetra ultricies, diam lorem lacinia risus, ac commodo orci erat eu massa. Sed sit amet nulla ipsum. Donec felis mauris, vulputate sed tempor at, aliquam a ligula. Pellentesque non pulvinar nisi.
						</p>
						</div>
						<div class="col-lg-6">
						<h3>And Paragraphs</h3>
						<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales at. Nulla tellus elit, varius non commodo eget, mattis vel eros. In sed ornare nulla. Donec consectetur, velit a pharetra ultricies, diam lorem lacinia risus, ac commodo orci erat eu massa. Sed sit amet nulla ipsum. Donec felis mauris, vulputate sed tempor at, aliquam a ligula. Pellentesque non pulvinar nisi.
						</p>
					  </div>
				  </div>
				  <div class="row">
					  <div class="col-lg-12">
						<h2>Example use of Tooltips</h2>
						<p>Hover over the links below to see tooltips:</p>
						<div class="tooltip-demo well">
						  <p class="muted" style="margin-bottom: 0;">Tight pants next level keffiyeh <a href="#" data-rel="tooltip" data-original-title="first tooltip">you probably</a> haven't heard of them. Photo booth beard raw denim letterpress vegan messenger bag stumptown. Farm-to-table seitan, mcsweeney's fixie sustainable quinoa 8-bit american appadata-rel <a href="#" data-rel="tooltip" data-original-title="Another tooltip">have a</a> terry richardson vinyl chambray. Beard stumptown, cardigans banh mi lomo thundercats. Tofu biodiesel williamsburg marfa, four loko mcsweeney's cleanse vegan chambray. A <a href="#" data-rel="tooltip" data-original-title="Another one here too">really ironic</a> artisan whatever keytar, scenester farm-to-table banksy Austin <a href="#" data-rel="tooltip" data-original-title="The last tip!">twitter handle</a> freegan cred raw denim single-origin coffee viral.
						  </p>
						</div>                                  
					  </div>
				  </div>	 
			  </div>
		</div>
	</div><!--/col-->
	
	<?php include_once "content-table.php"; ?>

</div><!--/row-->
