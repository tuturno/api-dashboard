<!-- Common Table content for all documentation -->
	<div class="col-lg-3">
		<div class="panel panel-default">
			<div class="panel-heading" data-original-title>
				<h2><i class="fa fa-list"></i><span class="break"></span>Content Table</h2>
			</div>
			<div class="panel-body">
				<ul>
				  <li>Lorem ipsum dolor sit amet</li>
				  <li>Consectetur adipiscing elit</li>
				  <li>Integer molestie lorem at massa</li>
				  <li>Facilisis in pretium nisl aliquet</li>
				  <li>Nulla volutpat aliquam velit
					<ul>
					  <li>Phasellus iaculis neque</li>
					  <li>Purus sodales ultricies</li>
					  <li>Vestibulum laoreet porttitor sem</li>
					  <li>Ac tristique libero volutpat at</li>
					</ul>
				  </li>
				  <li>Faucibus porta lacus fringilla vel</li>
				  <li>Aenean sit amet erat nunc</li>
				  <li>Eget porttitor lorem</li>
				</ul>            
			</div>
		</div>
	</div><!--/col-->