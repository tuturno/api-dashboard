<ul class="sidebar-actions">
	<li class="action">
		<div class="btn-group dropup">
		  	<button type="button" class="dropdown-toggle" data-toggle="dropdown">
		    	<i class="icon-speedometer"></i><span>Quick Stats</span>
		    	<span class="sr-only">Toggle Dropdown</span>
		  	</button>
		  	<ul class="dropdown-menu" role="menu">
				<li class="header">Monthly Stats <i class="icon-settings"></i></li>
		    	<li>
					<div class="title">API Calls<span>2.54k of 10m</span></div>
					<div class="progress">
						<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%"></div>
					</div>			
				</li>
				<li>
					<div class="title">Turns created<span>750 of 1000</span></div>
					<div class="progress">
					  	<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
					</div>
				</li>
				<li>
					<div class="title">Turns attended<span>684 of 1000</span></div>
					<div class="progress">
				  		<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%"></div>
					</div>			
				</li>
				<li>
					<div class="title">Users<span>10 of 10</span></div>
					<div class="progress">
				  		<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
					</div>
				</li>
			</ul>
		</div>
	</li>
	<li class="action">
		<div class="btn-group dropup">
		  	<button type="button" class="dropdown-toggle" data-toggle="dropdown">
		    	<i class="icon-list"></i><span>Menu</span>
		    	<span class="sr-only">Toggle Dropdown</span>
		  	</button>
		  	<ul class="dropdown-menu" role="menu">
		    	<li><a href="#">Action</a></li>
	          	<li><a href="#">Another action</a></li>
	          	<li><a href="#">Something else here</a></li>
	          	<li class="divider"></li>
	          	<li><a href="#">Separated link</a></li>
		  	</ul>
		</div>
	</li>
	<li class="action">
		<div class="btn-group dropup">
		  	<button type="button" class="dropdown-toggle" data-toggle="dropdown">
		    	<i class="icon-users"></i><span>Users</span>
		    	<span class="sr-only">Toggle Dropdown</span>
		  	</button>
		  	<ul class="dropdown-menu" role="menu">
		    	<li class="header">Users <i class="icon-settings"></i></li>
	          	<li><a href="#"><span class="status status-success"></span> Anton Phunihel</a></li>
	          	<li><a href="#"><span class="status status-success"></span> Alphonse Ivo</a></li>
	          	<li><a href="#"><span class="status status-success"></span> Thancmar Theophanes</a></li>
				<li><a href="#"><span class="status status-warning"></span> Walerian Khwaja</a></li>
				<li><a href="#"><span class="status status-warning"></span> Clemens Janko</a></li>
				<li><a href="#"><span class="status status-warning"></span> Chidubem Gottlob</a></li>
				<li><a href="#"><span class="status status-danger"></span> Hristofor Sergio</a></li>
				<li><a href="#"><span class="status status-danger"></span> Tadhg Griogair</a></li>
				<li><a href="#"><span class="status status-danger"></span> Pollux Beaumont</a></li>
				<li><a href="#"><span class="status status-danger"></span> Adam Alister</a></li>
				<li><a href="#"><span class="status status-danger"></span> Carlito Roffe</a></li>
		  	</ul>
		</div>
	</li>
</ul>