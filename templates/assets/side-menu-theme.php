<ul class="nav nav-sidebar">

	<li><a href="dashboard.html"><i class="icon-speedometer"></i><span class="text"> Dashboard<span class="label label-info">NEW</span></span></a></li>	

	<li>
		<a href="#"><i class="icon-magic-wand"></i><span class="text"> UI Features</span> <span class="indicator"></span></a>
		<ul>
			<li><a href="ui-sliders-progress.html"><i class="icon-magic-wand"></i><span class="text"> Sliders</span></a></li>
			<li><a href="ui-nestable-list.html"><i class="icon-magic-wand"></i><span class="text"> Nestable Lists</span></a></li>
			<li><a href="ui-elements.html"><i class="icon-magic-wand"></i><span class="text"> Elements</span></a></li>
			<li><a href="ui-panels.html"><i class="icon-magic-wand"></i><span class="text"> Panels <span class="label label-info">NEW</span></span></a></li>
			<li><a href="ui-buttons.html"><i class="icon-magic-wand"></i><span class="text"> Buttons <span class="label label-warning">NEW</span></span></a></li>
			<li><a href="ui-modals.html"><i class="icon-magic-wand"></i><span class="text"> Modals <span class="label label-info">NEW</span></span></a></li>
			<li><a href="ui-notifications.html"><i class="icon-magic-wand"></i><span class="text"> Notifications <span class="label label-info">NEW</span></span></a></li>
		</ul>
		</li>
	<li><a href="widgets.html"><i class="icon-calculator"></i><span class="text"> Widgets</span></a></li>

	<li>
		<a href="#"><i class="icon-book-open"></i><span class="text"> Example Pages</span> <span class="indicator"></span></a>
		<ul>
			<li><a href="page-invoice.html"><i class="icon-credit-card"></i><span class="text"> Invoice</span></a></li>
			<li><a href="page-todo.html"><i class="icon-list"></i><span class="text"> ToDo &amp; Timeline</span></a></li>
			<li><a href="page-profile.html"><i class="icon-user-following"></i><span class="text"> Profile</span></a></li>
			<li><a href="page-pricing-tables.html"><i class="icon-basket"></i><span class="text"> Pricing Tables</span></a></li>
			<li><a href="page-404.html" target="_top"><i class="icon-link"></i><span class="text"> 404</span></a></li>
			<li><a href="page-500.html" target="_top"><i class="icon-link"></i><span class="text"> 500</span></a></li>
			<li><a href="page-lockscreen.html" target="_top"><i class="icon-lock"></i><span class="text"> LockScreen</span></a></li>
			<li><a href="page-lockscreen2.html" target="_top"><i class="icon-lock"></i><span class="text"> LockScreen2</span></a></li>
			<li><a href="page-login" target="_top"><i class="icon-login"></i><span class="text"> Login <span class="label label-danger">NEW</span></span></a></li>
			<li><a href="page-register" target="_top"><i class="icon-logout"></i><span class="text"> Register <span class="label label-success">NEW</span></span></a></li>
			<li><a href="page-login" target="_top"><i class="icon-login"></i><span class="text"> Login Alt <span class="label label-danger">NEW</span></span></a></li>
			<li><a href="page-register" target="_top"><i class="icon-logout"></i><span class="text"> Register Alt <span class="label label-success">NEW</span></span></a></li>
			<li><a href="page-blank.html"><i class="icon-docs"></i><span class="text"> Blank Page</span></a></li>
		</ul>	
	</li>
	<li>
		<a href="#"><i class="icon-note"></i><span class="text"> Forms</span> <span class="indicator"></span></a>
		<ul>
			<li><a href="form-elements.html"><i class="icon-note"></i><span class="text"> Form elements</span></a></li>
			<li><a href="form-wizard.html"><i class="icon-note"></i><span class="text"> Wizard</span></a></li>
			<li><a href="form-dropzone.html"><i class="icon-note"></i><span class="text"> Dropzone Upload</span></a></li>
			<li><a href="form-x-editable.html"><i class="icon-note"></i><span class="text"> X-editable</span></a></li>
		</ul>
	</li>
	<li>
		<a href="#"><i class="icon-bar-chart"></i><span class="text"> Charts</span> <span class="indicator"></span></a>
		<ul>
			<li><a href="charts-flot.html"><i class="icon-bar-chart"></i><span class="text"> Flot Charts</span></a></li>
			<li><a href="charts-xcharts.html"><i class="icon-bar-chart"></i><span class="text"> xCharts</span></a></li>
			<li><a href="charts-others.html"><i class="icon-bar-chart"></i><span class="text"> Other</span></a></li>
		</ul>

	</li>
	<li><a href="typography.html"><i class="icon-pencil"></i><span class="text"> Typography</span></a></li>

	<li><a href="gallery.html"><i class="icon-picture"></i><span class="text"> Gallery</span></a></li>
	<li><a href="table.html"><i class="icon-grid"></i><span class="text"> Tables</span></a></li>
	<li><a href="calendar.html"><i class="icon-calendar"></i><span class="text"> Calendar</span></a></li>
	<li>
		<a href="#"><i class="icon-star"></i><span class="text"> Icons</span> <span class="indicator"></span></a>
		<ul>
			<li><a href="icons-halflings.html"><i class="icon-star"></i><span class="text"> Halflings</span></a></li>
			<li><a href="icons-glyphicons-pro.html"><i class="icon-star"></i><span class="text"> Glyphicons PRO</span></a></li>
			<li><a href="icons-filetypes.html"><i class="icon-star"></i><span class="text"> Filetypes</span></a></li>
			<li><a href="icons-social.html"><i class="icon-star"></i><span class="text"> Social</span></a></li>
			<li><a href="icons-font-awesome.html"><i class="icon-star"></i><span class="text"> Font Awesome</span></a></li>
			<li><a href="icons-climacons.html"><i class="icon-star"></i><span class="text"> Climacons</span></a></li>
			<li><a href="icons-simple-line-icons.html"><i class="icon-star"></i><span class="text"> Simple Line Icons</span></a></li>
		</ul>
	</li>
	<li>
		<a href="#"><i class="icon-folder-alt"></i><span class="text"> 4 Level Menu</span> <span class="indicator"></span></a>
		<ul>
			<li><a href="2nd-level.html"><i class="icon-folder"></i><span class="text"> 2nd Level</span></a></li>
			<li>
				<a href="#"><i class="icon-folder-alt"></i><span class="text"> 2nd Level</span> <span class="indicator"></span></a>
				<ul>
					<li><a href="3rd-level.html"><i class="icon-folder"></i><span class="text"> 3rd Level</span></a></li>
					<li>
						<a href="#"><i class="icon-folder-alt"></i><span class="text"> 3rd Level</span> <span class="indicator"></span></a>
						<ul>
							<li>
								<a href="4th-level.html"><i class="icon-folder"></i><span class="text"> 4th Level</span></a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#"><i class="icon-folder-alt"></i><span class="text"> 3rd Level</span> <span class="indicator"></span></a>
						<ul>
							<li>
								<a href="4th-level2.html"><i class="icon-folder"></i><span class="text"> 4th Level</span></a>
							</li>
						</ul>
					</li>
				</ul>	
			</li>
		</ul>
	</li>
</ul>