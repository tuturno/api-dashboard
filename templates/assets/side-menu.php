<ul class="nav nav-sidebar">

	<li><a href="dashboard.html"><i class="icon-speedometer"></i><span class="text"> Dashboard<span class="label label-info">Welcome</span></span></a></li>	

	<li><a href="settings-wizard.html"><i class="icon-magic-wand"></i><span class="text"> Settings Wizard</span></a></li>
	<li>
		<a href="#"><i class="icon-note"></i><span class="text"> Turns Settings</span> <span class="indicator"></span></a>
		<ul>
			<li><a href="companies.html"><i class="icon-calculator"></i><span class="text"> Companies</span></a></li>
			<li><a href="queues.html"><i class="icon-directions"></i><span class="text"> Queues</span></a></li>
			<li><a href="services.html"><i class="icon-calculator"></i><span class="text"> Services</span></a></li>
			<li><a href="settings.html"><i class="icon-settings"></i><span class="text"> Settings</span></a></li>
		</ul>
	</li>	

	<li>
		<a href="#"><i class="icon-book-open"></i><span class="text"> API Docs <span class="indicator"></span></span></a>
		<ul>
			<li><a href="docs/getting-started.php"><i class="icon-book-open"></i><span class="text"> Getting Started</span></a></li>
			<li><a href="docs/companies-queues.php"><i class="icon-book-open"></i><span class="text"> Companies and Queues</span></a></li>
			<li><a href="docs/turns-management.php"><i class="icon-book-open"></i><span class="text"> Turns Management</span></a></li>
			<li><a href="docs/alarms-settings.php"><i class="icon-book-open"></i><span class="text"> Alarms and Settings</span></a></li>
			
			<li>
				<a href="#"><i class="icon-folder-alt"></i><span class="text"> Examples</span> <span class="indicator"></span></a>
				<ul>
					<li><a href="2nd-level.html"><i class="icon-folder"></i><span class="text"> Java</span></a></li>
					<li><a href="3rd-level.html"><i class="icon-folder"></i><span class="text"> C#</span></a></li>
					<li><a href="4th-level.html"><i class="icon-folder"></i><span class="text"> PHP</span></a></li>
					<li><a href="4th-level2.html"><i class="icon-folder"></i><span class="text"> Javascript</span></a></li>
				</ul>
			</li>
		</ul>
	</li>
	
	<li>
		<a href="#"><i class="icon-bar-chart"></i><span class="text"> API Stats</span> <span class="indicator"></span></a>
		<ul>
			<li><a href="charts-flot.html"><i class="icon-bar-chart"></i><span class="text"> Flot Charts</span></a></li>
			<li><a href="charts-xcharts.html"><i class="icon-bar-chart"></i><span class="text"> xCharts</span></a></li>
			<li><a href="charts-others.html"><i class="icon-bar-chart"></i><span class="text"> Other</span></a></li>
		</ul>

	</li>

	<li><a href="contact-form.html"><i class="icon-question"></i><span class="text"> Help &amp; Support</span></a></li>

	<li><a href="page-profile.html"><i class="icon-user-following"></i><span class="text"> Your Account</span></a></li>
	<li><a href="page-invoice.html"><i class="icon-credit-card"></i><span class="text"> Billing</span></a></li>
	
</ul>