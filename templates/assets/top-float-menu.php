<ul class="dropdown-menu">
	<li class="dropdown-menu-header text-center">
		<strong>Account</strong>
	</li>
	<li><a href="#"><i class="fa fa-bell-o"></i> Updates <span class="label label-info">2</span></a></li>
	<li><a href="#"><i class="fa fa-envelope-o"></i> Messages <span class="label label-success">8</span></a></li>
	<li><a href="#"><i class="fa fa-tasks"></i> Pending Tasks <span class="label label-danger">5</span></a></li>
	<li class="dropdown-menu-header text-center">
		<strong>Settings</strong>
	</li>
	
	<li><a href="#"><i class="fa fa-usd"></i> Payments <span class="label label-default">42</span></a></li>
	<li class="divider"></li>
	<li><a href="<?php echo ROOT_URI; ?>/logout/" target="_top"><i class="fa fa-lock"></i> Logout</a></li>	
</ul>