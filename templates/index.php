<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="The Turns Management API">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="57x57" href="assets/ico/apple-touch-icon-57-precomposed.png">
		<link rel="shortcut icon" href="assets/ico/favicon.png">

	    <title>API Dashboard</title>

	    <!-- Bootstrap core CSS -->
	    <!-- <link href="assets/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-style"> -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
			
		<link href="assets/css/jquery.mmenu.css" rel="stylesheet">
		<link href="assets/css/simple-line-icons.css" rel="stylesheet">
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Bad+Script' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>

	    <!-- Custom styles for this template -->
	    <link href="assets/css/style.css" rel="stylesheet" id="main-style">
		<link href="assets/css/add-ons.css" rel="stylesheet">		

	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
	</head>
	
	<body id="DashboardApp" ng-app="DashboardApp">
		
		<!-- start: Header -->
		<div class="navbar" role="navigation">
			<div class="navbar-header">
				<a class="navbar-brand" href="/"><i class="icon-rocket"></i> <span class="logo">MobileTurns</span></a>
			</div>
			<ul class="nav navbar-nav navbar-actions navbar-left">
				<li class="visible-md visible-lg"><a href="#" id="main-menu-toggle"><i class="fa fa-bars"></i></a></li>
				<li class="visible-xs visible-sm"><a href="#" id="sidebar-menu"><i class="fa fa-bars"></i></a></li>
			</ul>
			<form class="navbar-form navbar-left">
				<i class="fa fa-search"></i>
				<input type="text" class="form-control" placeholder="Are you looking for something ?">
			</form>
	        <ul class="nav navbar-nav navbar-right visible-md visible-lg">
				<!-- <li><button class="btn btn-default">Preview</button></li>
				<li><button class="btn btn-success">Launch</button></li> -->
				<li><span class="timer"><i class="icon-clock"></i> <span id="clock"><!-- JavaScript clock will be displayed here, if you want to remove clock delete parent <li> --></span></span></li>
				<li class="dropdown visible-md visible-lg">
	        		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="assets/ico/flags/USA.png" style="height:18px; margin-top:-4px;"></a>
	        		<ul class="dropdown-menu">
						<li><a href="#"><img src="assets/ico/flags/USA.png" style="height:18px; margin-top:-2px;"> US</a></li>
						<li><a href="#"><img src="assets/ico/flags/Spain.png" style="height:18px; margin-top:-2px;"> Spanish</a></li>
						<li><a href="#"><img src="assets/ico/flags/Germany.png" style="height:18px; margin-top:-2px;"> German</a></li>
						<li><a href="#"><img src="assets/ico/flags/Poland.png" style="height:18px; margin-top:-2px;"> Polish</a></li>	
	        		</ul>
	      		</li>
				<li class="dropdown visible-md visible-lg">
	        		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-envelope-letter"></i><span class="badge">4</span></a>
	        		<ul class="dropdown-menu">
						<li class="dropdown-menu-header">
							<strong>Messages</strong>
							<div class="progress thin">
							  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
							    <span class="sr-only">40% Complete (success)</span>
							  </div>
							</div>
						</li>
						<li class="avatar">
							<a href="#">
								<img class="avatar" src="assets/img/avatar.jpg">
								<div>New message</div>
								<small>1 minute ago</small>
								<span class="label label-info">NEW</span>
							</a>
						</li>
						<li class="avatar">
							<a href="#">
								<img class="avatar" src="assets/img/avatar.jpg">
								<div>New message</div>
								<small>1 minute ago</small>
								<span class="label label-info">NEW</span>
							</a>
						</li>
						<li class="avatar">
							<a href="#">
								<img class="avatar" src="assets/img/avatar.jpg">
								<div>New message</div>
								<small>1 minute ago</small>
							</a>
						</li>
						<li class="avatar">
							<a href="#">
								<img class="avatar" src="assets/img/avatar.jpg">
								<div>New message</div>
								<small>1 minute ago</small>
							</a>
						</li>
						<li class="dropdown-menu-footer text-center">
							<a href="#">View all messages</a>
						</li>	
	        		</ul>
	      		</li>
				<li class="dropdown visible-md visible-lg">
	        		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-settings"></i><span class="badge">!</span></a>
	        		<?php require_once "assets/top-float-menu.php" ?>
	      		</li>
			</ul>
		</div>
		<!-- end: Header -->
		
		<!-- start: Main Menu -->
		<div class="sidebar">

			<div class="sidebar-collapse">

				<div class="sidebar-header">

					<img src="assets/img/avatar9.jpg">

					<h2>John Doe</h2>
					<h3>john@doe.com <a href="#"><i class="fa fa-chevron-down"></i></a></h3>

				</div>

				<div class="sidebar-menu">	
					<?php require_once "assets/side-menu.php" ?>
				</div>					
			</div>
			<div class="sidebar-footer">
				<?php require_once "assets/side-footer-actions.php" ?>

				<ul class="sidebar-terms">
					<li><a href="#">Terms</a></li>
					<li><a href="#">Privacy</a></li>
					<li><a href="#">Help</a></li>
					<li><a href="#">About</a></li>
				</ul>	

			</div>	
		</div>
		<!-- end: Main Menu -->

		<!-- start: Content -->
		<div class="main">

		</div>
		<!-- end: Content -->

		<footer>

			<div class="row">

				<div class="col-sm-5">
					&copy; <?php echo date("Y") ?> SixTI Group. All Rights Reserved.
				</div><!--/.col-->

				<div class="col-sm-7 text-right">
					Designed by: <a href="http://smadit.com" alt="Smad IT">Smad IT</a> | Based on Bootstrap 3.3.2 | Built with <a href="http://brix.io" alt="Brix.io">Brix.io</a>
				</div><!--/.col-->	

			</div><!--/.row-->	

		</footer>

		<div class="modal fade" id="myModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Modal title</h4>
					</div>
					<div class="modal-body">
						<p>Here settings can be configured...</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary">Save changes</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!-- start: JavaScript-->
		<!--[if !IE]>-->
				<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<!--<![endif]-->

		<!--[if IE]>
			<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
		<![endif]-->

		<!--[if !IE]>-->
			<script type="text/javascript">
				window.jQuery || document.write("<script src='assets/js/jquery-2.1.1.min.js'>"+"<"+"/script>");
			</script>
		<!--<![endif]-->

		<!--[if IE]>
			<script type="text/javascript">
		 	window.jQuery || document.write("<script src='assets/js/jquery-1.11.1.min.js'>"+"<"+"/script>");
			</script>
		<![endif]-->
		
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.2/angular.min.js"></script>
		<script src="https://code.angularjs.org/1.4.2/angular-route.min.js"></script>
		

		<!-- theme scripts -->
		<script src="assets/js/app.js"></script>
		<script src="assets/plugins/pace/pace.min.js"></script>
		<script src="assets/js/jquery.mmenu.min.js"></script>
		<script src="assets/js/core.min.js"></script>
		<script src="assets/plugins/jquery-cookie/jquery.cookie.min.js"></script>

		<!-- end: JavaScript-->

	</body>
</html>