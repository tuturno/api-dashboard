<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="Login | The Turns Management API">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="57x57" href="assets/ico/apple-touch-icon-57-precomposed.png">
		<link rel="shortcut icon" href="assets/ico/favicon.png">

	    <title>Login | MobileTurns - The Turns Management API</title>

	    <!-- Bootstrap core CSS -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" id="bootstrap-style">
			
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Bad+Script' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>

	    <!-- Custom styles for this template -->
	    <link href="assets/css/style.css" rel="stylesheet" id="main-style">
		
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
	</head>

<body class="login">
	<div class="container">
		<div class="row">
			<div class="login-box col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
				
				<div class="header">
					<a href="http://mobileturns.com"><h1 class="logo">MobileTurns</h1></a>
					<span class="tagline">The Turns Management API</span>
				</div>
				
				<form action="<?php echo ROOT_URI; ?>/login/process/" method="post">
					
					<fieldset>
						<div id="info-msg" class="alert alert-info hidden" role="alert">
							<?php echo $flash['info']; ?>
						</div>
						
						<div class="form-group first">
						  	<div class="input-group col-sm-12">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<input type="text" required class="form-control input-lg" id="username" placeholder="Username or E-mail"/>	
							</div>
						</div>
						
						<div class="form-group last">
						  	<div class="input-group col-sm-12">
								<span class="input-group-addon"><i class="fa fa-key"></i></span>
								<input type="password" required class="form-control input-lg" id="password" placeholder="Password"/>
							</div>
						</div>
	
						<button type="submit" class="btn btn-primary col-xs-12">Login</button>

						<p class="small text-center">OR</p>

						<div class="row">
							<div class="col-xs-12">
								<a href="/register" class="btn btn-sm  btn-block btn-openid"><span>Create a New Account</span></a>
							</div>
						</div>
						
						<?php
						/*
						<div class="row social">
						
							<div class="col-xs-4">
								<a href="#" class="btn btn-sm btn-block btn-facebook"><span>Facebook</span></a>
							</div><!--/col-->
							
							<div class="col-xs-4">
								<a href="#" class="btn btn-sm  btn-block btn-twitter"><span>Twitter</span></a>
							</div><!--/col-->
							
							<div class="col-xs-4">
								<a href="#" class="btn btn-sm btn-block btn-linkedin"><span>LinkedIn</span></a>
							</div><!--/col-->
						
						</div><!--/row-->
						
						<p class="row">
						
							<div class="col-xs-5">
								<a class="pull-right" target="_top" href="/register">Register</a>
							</div><!--/col-->
							
							<div class="col-xs-7">
								<a class="pull-left" href="#">Forgot Password?</a>
							</div><!--/col-->
						
						</p><!--/row-->
						*/
						?>
							
					</fieldset>	

				</form>
						
			</div>
		</div><!--/row-->	
	</div><!--/container-->	
		
	<!-- start: JavaScript-->
	<!--[if !IE]>-->

			<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

	<!--<![endif]-->

	<!--[if IE]>
	
		<script src="assets/js/jquery-1.11.1.min.js"></script>
	
	<![endif]-->

	<!--[if !IE]>-->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.1.1.min.js'>"+"<"+"/script>");
		</script>

	<!--<![endif]-->

	<!--[if IE]>
	
		<script type="text/javascript">
	 	window.jQuery || document.write("<script src='assets/js/jquery-1.11.1.min.js'>"+"<"+"/script>");
		</script>
		
	<![endif]-->
	<script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	
	
	<!-- inline scripts related to this page -->
	<script src="assets/js/pages/login.js"></script>
	
	<!-- end: JavaScript-->
	
</body>
</html>