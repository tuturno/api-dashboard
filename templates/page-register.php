<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="Register | The Turns Management API">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="57x57" href="assets/ico/apple-touch-icon-57-precomposed.png">
		<link rel="shortcut icon" href="assets/ico/favicon.png">

	    <title>Register | MobileTurns - The Turns Management API</title>

	    <!-- Bootstrap core CSS -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" id="bootstrap-style">
	
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Bad+Script' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>

	    <!-- Custom styles for this template -->
	    <link href="assets/css/style.css" rel="stylesheet" id="main-style">

	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
	</head>

<body class="login">
	<div class="container">
		<div class="row">
			<div class="login-box col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
				
				<div class="header">
					<a href="http://mobileturns.com"><h1 class="logo">MobileTurns</h1></a>
					<span class="tagline">The Turns Management API</span>
				</div>
				
				<form action="/register/process/" method="post">
					
					<fieldset>

						<div id="error-msg" class="alert alert-danger hidden" role="alert">
							<?php echo $flash['error']; ?>
						</div>

						<div class="form-group first">
						  	<div class="input-group col-sm-12">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<input type="text" required class="form-control input-lg" 
										id="fullname" name="fullname" placeholder="Full name"
										value="<?php echo $flash['fullName']; ?>"/>	
							</div>
						</div>
						
						<div class="form-group second">
						  	<div class="input-group col-sm-12">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<input type="text" required class="form-control input-lg" 
										id="username" name="username" placeholder="Username"
										value="<?php echo $flash['username']; ?>"/>	
							</div>
						</div>
						
						<div class="form-group middle">
						  	<div class="input-group col-sm-12">
								<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
								<input type="email" required class="form-control input-lg" 
										id="email" name="email" placeholder="E-mail"
										value="<?php echo $flash['email']; ?>"/>	
							</div>
						</div>
						
						<div class="form-group last">
						  	<div class="input-group col-sm-12">
								<span class="input-group-addon last"><i class="fa fa-key"></i></span>
								<input type="password" required class="form-control input-lg" 
										id="userpass" name="userpass" placeholder="Password"/>
							</div>

							<div class="input-group" style="position:absolute;top:-99999px;">
								<input type="text" class="form-control"
										id="userfiller" name="userfiller" placeholder="Do you want to fill this?">
							</div>
						</div>
	
						<button type="submit" class="btn btn-primary col-xs-12">Create Account</button>
						
						<p class="small text-center">OR</p>
						
						<div class="row">
							<div class="col-xs-12">
								<a target="_top" href="/login" class="btn btn-sm  btn-block btn-openid"><span>Login with existing account</span></a>
							</div>
						</div>
							
					</fieldset>	

				</form>
						
			</div>
		</div><!--/row-->	
	</div><!--/container-->	
		
	<!-- start: JavaScript-->
	<!--[if !IE]>-->

			<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

	<!--<![endif]-->

	<!--[if IE]>
	
		<script src="/assets/js/jquery-1.11.1.min.js"></script>
	
	<![endif]-->

	<!--[if !IE]>-->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.1.1.min.js'>"+"<"+"/script>");
		</script>

	<!--<![endif]-->

	<!--[if IE]>
	
		<script type="text/javascript">
	 	window.jQuery || document.write("<script src='assets/js/jquery-1.11.1.min.js'>"+"<"+"/script>");
		</script>
		
	<![endif]-->
	<script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	
	
	<!-- page scripts -->
	
	<!-- theme scripts -->
	<script src="assets/plugins/pace/pace.min.js"></script>
	<script src="assets/js/jquery.mmenu.min.js"></script>
	<script src="assets/js/core.min.js"></script>
	<script src="assets/plugins/jquery-cookie/jquery.cookie.min.js"></script>
	<!-- <script src="/assets/js/demo.min.js"></script> -->
	
	<!-- inline scripts related to this page -->
	<script src="assets/js/pages/login.js"></script>
	
	<!-- end: JavaScript-->
	
</body>
</html>